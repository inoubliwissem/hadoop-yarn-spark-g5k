#!/bin/bash

set -ueo pipefail

# Nodes deployment
kadeploy3 -f <(./oar_get_nodes.sh) -e debian10-x64-std -k 

# Setup reservable disk
clush --hostfile=<(./oar_get_nodes.sh) ${PWD}/setup_extra_disk.sh

# Setup basic tree structure for the experiment
./setup_xp.sh

# Master installation
ssh $(./oar_get_master.sh) ${PWD}/install_hadoop_on_master.sh

# Workers installation
ssh $(./oar_get_master.sh) ${PWD}/install_hadoop_on_workers.sh 

# Format HDFS
ssh $(./oar_get_master.sh) 'source /xp/.xpenv; hdfs namenode -format'

# Start HDFS
ssh $(./oar_get_master.sh) 'source /xp/.xpenv; start-dfs.sh'

# Start YARN
ssh $(./oar_get_master.sh) 'source /xp/.xpenv; start-yarn.sh'

# Create home dir in HDFS
ssh $(./oar_get_master.sh) 'source /xp/.xpenv; hdfs dfs -mkdir -p /user/$(id -un)'

# Spark installation
ssh $(./oar_get_master.sh) ${PWD}/install_spark_on_master.sh
ssh $(./oar_get_master.sh) 'source /xp/.xpenv; $SPARK_HOME/sbin/start-history-server.sh'

