#!/bin/bash

set -u

if [ ! -f /xp/etc/master ] || [ $(hostname -f) != $(cat /xp/etc/master) ] ; then
  echo "Must run on master"
  exit 1
fi

HERE=$(realpath $(dirname $0))
. $HERE/.xpenv
. $HERE/.utils.sh

cd /xp

echo "Get Hadoop ${HADOOP_VERSION} and install it"
wget -q https://downloads.apache.org/hadoop/common/hadoop-${HADOOP_VERSION}/hadoop-${HADOOP_VERSION}.tar.gz || die "cannot download hadoop ${HADOOP_VERSION}"
tar -xzf hadoop-${HADOOP_VERSION}.tar.gz || die "cannot extract hadoop ${HADOOP_VERSION}"
mv hadoop-${HADOOP_VERSION} hadoop

((echo -n "<% master_node='$(cat /xp/etc/master)'; %>" && cat $HERE/templates/core-site.xml) | erb > /xp/hadoop/etc/hadoop/core-site.xml) || die "cannot apply templates on hadoop/core-site.xml"
((echo -n "<% hadoop_data='/xp/hadoop/data'; %>" && cat $HERE/templates/hdfs-site.xml) | erb > /xp/hadoop/etc/hadoop/hdfs-site.xml) || die "cannot apply templates on hadoop/hdfs-site.xml"
((echo -n "<% map_reduce_am_resource_mb=${MAP_REDUCE_AM_RESOURCE_MB}; \
              map_memory_mb=${MAP_MEMORY_MB}; \
              reduce_memory_mb=${REDUCE_MEMORY_MB} %>" && cat $HERE/templates/mapred-site.xml) | erb > /xp/hadoop/etc/hadoop/mapred-site.xml) || die "cannot apply templates on hadoop/mapred-site.xml"
((echo -n "<% master_node='$(cat /xp/etc/master)'; \
              resource_memory_mb=${RESOURCE_MEMORY_MB}; \
              max_allocation_mb=${MAX_ALLOCATION_MB}; \
              min_allocation_mb=${MIN_ALLOCATION_MB} %>" && cat $HERE/templates/yarn-site.xml) | erb > /xp/hadoop/etc/hadoop/yarn-site.xml) || die  "cannot apply templates in hadoop/yarn-site.xml"

cp /xp/etc/workers /xp/hadoop/etc/hadoop/workers
cp /xp/etc/workers /xp/hadoop/etc/hadoop/slaves

echo "Set global environment variables on master node"
cp $HERE/.xpenv /xp/.xpenv
grep 'export JAVA_HOME' /xp/.xpenv >> /xp/hadoop/etc/hadoop/hadoop-env.sh
sudo cp /xp/.xpenv /etc/profile.d/xpenv.sh

echo "Get Java8 and install it"
wget -qO jdk.tgz $JAVA8_URL  || die "cannot download java from $JAVA8_URL"
tar xzf jdk.tgz || die "cannot extract java"
mv jdk${JAVA8_VERSION} java
chmod -R u+w java

echo "Copy book example"
cp -r $HERE/data/examples/books /xp/books

exit 0
