#!/bin/bash

set -u

sudo-g5k parted -s /dev/sdb mklabel gpt mkpart primary ext4 1MiB 100%
sudo-g5k mkfs.ext4 /dev/sdb1
sudo-g5k mkdir /xp
echo "/dev/sdb1 /xp ext4 defaults 0 0"  | sudo-g5k tee -a /etc/fstab
sudo-g5k mount /xp
sudo-g5k chown $(id -un):$(id -gn) /xp
