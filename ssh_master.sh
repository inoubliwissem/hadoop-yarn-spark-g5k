#!/bin/bash

set -ueo pipefail

ssh -t $($(dirname $0)/oar_get_master.sh) "cd /xp && bash -l"
