#!/bin/bash

set -u

if [ $(hostname -f) != $(cat /xp/etc/master) ] ; then
  echo "Must run on master"
  exit 1
fi

workers=/xp/etc/workers

clush --hostfile=$workers -l root --copy /xp/.xpenv --dest /etc/profile.d/xpenv.sh
clush --hostfile=$workers --copy /xp/hadoop --dest /xp
clush --hostfile=$workers --copy /xp/java --dest /xp

