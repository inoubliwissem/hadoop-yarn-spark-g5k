#!/bin/bash

set -u

oarstat -j $($(dirname $0)/oar_get_jobid.sh) -J  | jq -r '.[].assigned_network_address|.[]'
