#!/bin/bash

set -u 

HERE=$(realpath $(dirname $0))

. $HERE/.envrc

nodes=$(mktemp /tmp/nodes.XXXXXX)
master=$(mktemp /tmp/master.XXXXXX)
workers=$(mktemp /tmp/workers.XXXXXX)

$HERE/oar_get_nodes.sh > $nodes
$HERE/oar_get_master.sh > $master
$HERE/oar_get_workers.sh > $workers

clush --hostfile $nodes mkdir /xp/etc
clush --hostfile $nodes --copy $master --dest /xp/etc/master
clush --hostfile $nodes --copy $workers --dest /xp/etc/workers

rm $nodes $master $workers
