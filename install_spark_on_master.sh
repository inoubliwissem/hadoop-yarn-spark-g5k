#!/bin/bash

set -u

if [ ! -f /xp/etc/master ] || [ $(hostname -f) != $(cat /xp/etc/master) ] ; then
  echo "Must run on master"
  exit 1
fi

HERE=$(realpath $(dirname $0))

. $HERE/.xpenv
. $HERE/.utils.sh

cd /xp

echo "Get Spark $SPARK_VERSION and install it"
wget -q https://miroir.univ-lorraine.fr/apache/spark/spark-$SPARK_VERSION/$SPARK_FILE_NAME || die "cannot download $SPARK_FILE_NAME"
tar -xzf $SPARK_FILE_NAME || die "cannot extract Spark"
mv ${SPARK_FILE_NAME%.tgz} spark
((echo -n "<% master_node='$(cat /xp/etc/master)'; \
             spark_executor_memory='${SPARK_EXECUTOR_MEMORY}'; \
             spark_am_memory='${SPARK_AM_MEMORY}'; \
             spark_driver_memory='${SPARK_DRIVER_MEMORY}' %>" && cat $HERE/templates/spark-defaults.conf) | erb > /xp/spark/conf/spark-defaults.conf) || die "cannot apply templates on conf/spark-defaults.conf"

echo "Creating spark-logs in HDFS"
hdfs dfs -mkdir /spark-logs

exit 0
