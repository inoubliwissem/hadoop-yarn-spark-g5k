#!/bin/bash

set -u

oarstat -u $(id -un) -J | jq -r --arg MY_XP_NAME "${MY_XP_NAME}" 'first(.[]|select(.name==$MY_XP_NAME and .state=="Running")|.Job_Id)'
