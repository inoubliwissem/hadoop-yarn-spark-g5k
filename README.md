# Hadoop/SPARK deployment

## Warning

All the material in this repository is subject to change/evolve quickly. 

The idea is to find iteration after iteration on real cases what can be factorized and how to integrate the specific parts of user experiments.

## Documentation

- https://www.linode.com/docs/databases/hadoop/how-to-install-and-set-up-hadoop-cluster/
- https://www.linode.com/docs/databases/hadoop/install-configure-run-spark-on-top-of-hadoop-yarn-cluster/
- https://cwiki.apache.org/confluence/display/HADOOP/Hadoop+Java+Versions
- https://www.grid5000.fr/w/Disk_reservation#Reserve_disks_and_nodes_at_the_same_time

## Pre-requisites

- direnv (because we use a .envrc). `man direnv` to see how to integrate direnv in `.bashrc`

## Reservation
```
# On gros (default queue)
oarsub -n $MY_XP_NAME -t deploy -l {"(type='disk' or type='default') and cluster='gros'"}/host=4,walltime=2 "sleep 2h"
# On grappe (production queue)
oarsub -n $MY_XP_NAME -t deploy -l {"(type='disk' or type='default') and cluster='grappe'"}/host=4,walltime=2 "sleep 2h" -q production
```

# Deployment
```
kadeploy3 -f <(./oar_get_nodes.sh) -e debian10-x64-std -k 
```

Just type:

```
./setup.sh
```

or do the following steps:

# Reserved disks initialization
```
clush --hostfile=<(./oar_get_nodes.sh) ${PWD}/setup_extra_disk.sh
```

# Make the minimal tree structure required by the experiment
```
./setup_xp.sh

```

# Master installation
```
ssh $(./oar_get_master.sh) ${PWD}/install_hadoop_on_master.sh
```

## Workers installation
```
ssh $(./oar_get_master.sh) ${PWD}/install_hadoop_on_workers.sh 
```

## Format HDFS
```
./ssh_master.sh
hdfs namenode -format
```

## Start HDFS
```
start-dfs.sh
jps # Just to ckech the currently running processes
```

## Start yarn
```
start-yarn.sh
jps # Just to ckech the currently running processes
```

## Tests
```
hdfs dfs -mkdir -p /user/$(id -un)
hdfs dfs -mkdir books
hdfs dfs -put /xp/books/alice.txt /xp/books/holmes.txt /xp/books/frankenstein.txt books
hdfs dfs -ls books
hdfs dfs -get books/alice.txt
hdfs dfs -cat books/alice.txt
yarn jar /xp/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-examples-${HADOOP_VERSION}.jar wordcount "books/*" output
hdfs dfs -cat output/part-r-00000|sort --key 2 -n
```

## Spark Installation
```
ssh $(./oar_get_master.sh) ${PWD}/install_spark_on_master.sh
$SPARK_HOME/sbin/start-history-server.sh
```

## Tests Spark
```
spark-submit --deploy-mode cluster --class org.apache.spark.examples.SparkPi $SPARK_HOME/examples/jars/spark-examples_2.11-$SPARK_VERSION.jar 10
```

## Some useful commands or url
```
hdfs dfsadmin -report
hadoop port = master:9870
yarn port = master:8088
spark port = master:4040
spark-history port = master:18080
clush --hostfile=<(./oar_get_nodes.sh) jps
clush --hostfile=<(./oar_get_nodes.sh) -l root --copy $MY_XP_DIR/.xpenv --dest /etc/profile.d/xpenv.sh
```
